<!DOCTYPE html>
<html>
<head>
    <title>Index</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="scripts/jquery.min.js"></script>
    <script src="scripts/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $.get("http://172.22.100.131:3000/api/candidateVote", function(data, status){
                for(var i=0;i<data.length;i++){
                    $("#candidate").append("<option value='"+data[i].id+"'>"+data[i].desc+"</option>");
                }
            });
            $.get("http://172.22.100.131:3000/api/voter", function(data, status){
                $("#voter").html("");
                $.get("http://172.22.100.131:3000/api/ifVoted",function (data2, status2) {
                    for(var i=0;i<data.length;i++){
                        if(!data2[i].votedOrNot){
                            $("#voter").append("<option value='"+data[i].voterID+"'>"+data[i].fullName+"</option>");
                        }
                    }
                });
            });
            $("#voteBT").click(function(){
                $.post("http://172.22.100.131:3000/api/vote",
                {
                    "$class": "org.acme.voting.vote",
                    "candidateVoteAsset": "resource:org.acme.voting.candidateVote#"+$("#candidate").val(),
                    "ifVotedAsset": "resource:org.acme.voting.ifVoted#"+$("#voter").val()
                },
                function(data,status){
                    if(status=="success"){
                        alert("ลงคะแนนสำเร็จ");
                        $.get("http://172.22.100.131:3000/api/voter", function(data, status){
                            $("#voter").html("");
                            $.get("http://172.22.100.131:3000/api/ifVoted",function (data2, status2) {
                                for(var i=0;i<data.length;i++){
                                    if(!data2[i].votedOrNot){
                                        $("#voter").append("<option value='"+data[i].voterID+"'>"+data[i].fullName+"</option>");
                                    }
                                }
                            });
                        });
                    }
                    else{
                        alert("ท่านได้ลงคะแนนไปแล้ว");
                    }
                });
            });
        });
    </script>
</head>
<body>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                </button>
                <a class="navbar-brand" href="#">Logo</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Projects</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <form>
            <div class="form-group">
                <label for="candidate">candidate :</label>
                <select name="candidate" id="candidate" class="form-control">
                </select>
            </div>
            <div class="form-group">
                <label for="voter">voter :</label>
                <select name="voter" id="voter" class="form-control">
                </select>
            </div>
            <button type="button" class="btn btn-default" id="voteBT">Submit</button>
        </form>
    </div>
    <footer class="container-fluid text-center">
        <p>Footer Text</p>
    </footer>
</body>
</html>
